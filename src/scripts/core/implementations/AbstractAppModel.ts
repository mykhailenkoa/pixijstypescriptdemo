namespace core.implementations
{
    export abstract class AbstractAppModel implements IModel
    {

        constructor()
        {
            this.initialize();
        }
        protected initialize():void{
            let htmlElement:HTMLElement = document.getElementById(this.canvasHTMLElementDivName);
            this._canvasWidth = htmlElement.offsetWidth;
            this._canvasHeight = htmlElement.offsetHeight;
        }
        /**
         * @inheritDoc
         */
        isAppActive:boolean;

        /**
         * @inheritDoc
         */
        protected _canvasWidth:number;
        public get canvasWidth(): number{return this._canvasWidth;}

        /**
         * @inheritDoc
         */
        protected _canvasHeight:number;
        public get canvasHeight():number{return this._canvasHeight;}

        /**
         * @inheritDoc
         */
        public canvasResize(width: number, height: number): void
        {
            this._canvasWidth = width;
            this._canvasHeight = height;
        }

        public abstract get canvasHTMLElementDivName(): string;
    }
}