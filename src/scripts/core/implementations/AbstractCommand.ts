namespace core.implementations
{

    export abstract class AbstractCommand implements ICommand
    {
        /**
         * Getter
         * @returns {core.interfaces.IView} Application view implementation instance.
         */
        protected abstract get view(): IView;

        /**
         *  Setter
         * @param {core.interfaces.IView} value Application view implementation instance.
         */
        protected abstract set view(value: IView);

        /**
         * Getter
         * @returns {core.interfaces.IModel} Application model implementation instance.
         */
        protected abstract get model():IModel;
        /**
         * Setter
         * @param {core.interfaces.IModel} value Application model implementation instance.
         */
        protected abstract set model(value: IModel);

        /**
         * Constructor.
         * @param {core.interfaces.IModel} model
         * @param {core.interfaces.IView} view
         */
        constructor(model: IModel, view: IView)
        {
            this.initialize(model, view);
        }
        protected initialize(model: IModel, view: IView): void{
            this.model = model;
            this.view = view;
        }
        /**
         * Execute command instance.
         */
        public abstract execute(): void;

        /**
         * Dispose allocated command instance.
         */
        public abstract dispose(): void;

        public abstract get IsStayInMemory(): boolean;
    }
}