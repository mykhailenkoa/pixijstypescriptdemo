namespace core.implementations
{
    export abstract class AbstractAppContext implements IAppContext
    {
        enterFrameUpdate(): void {
        }

        public  abstract getModel(): IModel;
        public  abstract getView(): IView;
        public  abstract getController(): IController;

        /**
         * Register core application commands.
         */
        protected registerCoreCommands(replacementMap?:Map<string,Function>):void
        {
            let controller:IController = this.getController();// get context controller.
            let cmdNS = core.implementations.commands;

            //register commands
            controller.registerCommand(cmdNS.APP_RESIZE_CMD,
                AbstractAppContext.replaceOrGetDefault(cmdNS.APP_RESIZE_CMD,cmdNS.AppResizeCommand,replacementMap));

            controller.registerCommand(cmdNS.APP_ACTIVATED,
                AbstractAppContext.replaceOrGetDefault(cmdNS.APP_ACTIVATED,cmdNS.AppActivated,replacementMap));

            controller.registerCommand(cmdNS.APP_DEACTIVATED,
                AbstractAppContext.replaceOrGetDefault(cmdNS.APP_DEACTIVATED,cmdNS.AppDeactivated,replacementMap));

            controller.registerCommand(cmdNS.APP_VIEW_REDRAW,
                AbstractAppContext.replaceOrGetDefault(cmdNS.APP_VIEW_REDRAW,cmdNS.AppCoreViewRedrawCommand,replacementMap));

        }
        private static replaceOrGetDefault(name:string,def:Function,replacementMap?:Map<string,Function>):Function
        {
            return replacementMap&&replacementMap.has(name)?replacementMap.get(name):def;
        }
    }
}