declare namespace core.implementations.commands {
    class AppActivated extends AbstractCommand {
        execute(): void;
        dispose(): void;
    }
}
