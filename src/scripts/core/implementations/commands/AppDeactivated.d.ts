declare namespace core.implementations.commands {
    class AppDeactivated extends AbstractCommand {
        execute(): void;
        dispose(): void;
    }
}
