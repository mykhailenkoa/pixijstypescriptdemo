declare namespace core.implementations {
    import IController = core.interfaces.IController;
    import IAppContext = core.interfaces.IAppContext;
    abstract class AbstractController implements IController {
        protected abstract _appContext: IAppContext;
        private _commandMap;
        private _commandsInMemory;
        constructor();
        registerCommand(name: string, classImpl: Function): void;
        hasCommand(name: string): boolean;
        executeCommand(name: string, stayInMemory: boolean): void;
        removeComand(name: string): void;
        dispose(): void;
    }
}
