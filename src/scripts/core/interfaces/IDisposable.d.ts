declare namespace core.interfaces {
    interface IDisposable {
        dispose(): void;
    }
}
