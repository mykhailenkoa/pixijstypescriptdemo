namespace core.interfaces
{
    export interface IDisposable
    {
        /**
         * Dispose allocated data.
         */
        dispose():void;
    }
}