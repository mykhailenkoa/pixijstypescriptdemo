declare namespace core.interfaces {
    interface IView extends IDisposable {
        resize(w: number, h: number): void;
        onEnterFrameRedraw(): void;
    }
}
