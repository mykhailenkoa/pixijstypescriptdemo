namespace core.interfaces
{
    export interface IModel
    {
        /**
         * Resize application.
         * @param {number} width set app canvas width.
         * @param {number} height set app canvas height
         */
        canvasResize(width:number, height:number):void;

        /**
         * Predefined div name, for this application in a html document.
         * @returns {string}
         */
        canvasHTMLElementDivName:string;

        /**
         * Application canvas width.
         */
        canvasWidth:number;

        /**
         * Application canvas height.
         */
        canvasHeight:number;

        /**
         * Flag, true when application active.
         */
        isAppActive:boolean;
    }
}