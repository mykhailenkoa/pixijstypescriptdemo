namespace core.interfaces
{
    export interface IAppContext
    {
        getModel(): IModel;
        getView(): IView;
        getController(): IController;
        enterFrameUpdate():void;
    }
}