namespace core.interfaces
{
    /**
     * core Controller interface, comand executer,
     * generate bisness command from model
     * and send action for update view.
     */
    export interface IController extends IDisposable
    {
        /**
         *  Register command implementation.
         *  @param {string} name - current command name
         *  @param {Function} classImpl - may be any <code>ICommand</code> implementor
         */
        registerCommand( name:string, classImpl:Function):void;

        /**
         * Find registered command implementator by name.
         * @param {string} name - command name to search.
         * @returns {boolean} - indicates the existence of a previously registered command.
         */
        hasCommand( name:string,  ):boolean;

        /**
         * Find and execute registered command.
         * @param {string} name - find existing command and execute them.
         */
        executeCommand( name:string ):void;

        /**
         * Remove command by name
         * @param {string} name existen registered command name
         */
        removeCommand( name:string ):void;
    }
}