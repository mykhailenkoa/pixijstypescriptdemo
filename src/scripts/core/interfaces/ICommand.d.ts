declare namespace core.interfaces {
    interface ICommand extends IDisposable {
        execute(): void;
    }
}
