namespace core.interfaces
{
    export interface IView extends IDisposable
    {
        /**
         * Resize current application view.
         * @param {number} w app canvas width.
         * @param {number} h app canvas height.
         */
        resize(w:number, h:number):void;

        /**
         * Redraw application view, executes 60 times per second.
         */
        onEnterFrameRedraw():void;

        /**
         * Set enabled application view.
         * @param {Boolean} value
         */
        setEnabled( value:Boolean );
    }
}