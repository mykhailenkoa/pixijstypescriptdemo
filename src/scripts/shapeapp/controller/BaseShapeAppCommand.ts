import AbstractCommand = core.implementations.AbstractCommand;
import IView = core.interfaces.IView;

namespace shapeapp.controller
{

    import PixiEngineViewExt = shapeapp.view.PixiEngineViewExt;

    export class BaseShapeAppCommand extends AbstractCommand
    {
        /**
         * @inheritDoc
         */
        constructor(model: IModel, view: IView)
        {
            super(model,view);
            let t:string = typeof this;

            if (t == "BaseCoreCommand")
                throw new Error("BaseCoreCommand can't an instance, because this is a base class, please use his extend classes for instance. ");
        }
        private _view:PixiEngineViewExt;
        /**
         * @inheritDoc
         */
        protected get view(): PixiEngineViewExt{return this._view;}
        /**
         * @inheritDoc
         */
        protected set view(value:PixiEngineViewExt){this._view = value;}

        private _model:ShapeAppModel;
        /**
         * @inheritDoc
         */
        protected get model(): ShapeAppModel {return this._model;}
        /**
         * @inheritDoc
         */
        protected set model(value:ShapeAppModel){this._model=value;}

        /**
         * @inheritDoc
         */
        public execute(): void{}
        /**
         * @inheritDoc
         */
        public dispose(): void
        {
            this._view=null;
            this._model=null;
        }

        public get IsStayInMemory(): boolean{return true;}

    }
}