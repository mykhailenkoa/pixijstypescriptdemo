namespace shapeapp.controller
{
    export const CREATE_RND_SHAPE_BY_DELAY:string = "shapeapp.cmd.CREATE_RND_SHAPE_BY_DELAY";
    export const CREATE_RND_SHAPE_PER_SECOND_DELAY:string = "shapeapp.cmd.CREATE_RND_SHAPE_PER_SECOND_DELAY";
    export const SHAPES_COUNT_DECREASE_ATCLICK:string = "shapeapp.cmd.SHAPES_COUNT_DECREASE_ATCLICK";
    export const SHAPES_COUNT_INCREASE_ATCLICK:string = "shapeapp.cmd.SHAPES_COUNT_INCREASE_ATCLICK";
    export const GRAVITY_COUNT_DECREASE:string = "shapeapp.cmd.GRAVITY_COUNT_DECREASE";
    export const GRAVITY_COUNT_INCREASE:string = "shapeapp.cmd.GRAVITY_COUNT_INCREASE";
}