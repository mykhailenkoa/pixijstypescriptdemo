namespace shapeapp.controller.commands
{
    export class ShapeAppFrameUpdateCommand extends BaseShapeAppCommand
    {
        execute():void
        {
            this.model.calculateBeforeViewRender();
            this.view.onEnterFrameRedraw();
            this.model.calculateAfterViewRender();

            this.view.updateDisplayFPS(this.model.calculatedFPS);
        }
    }
}