namespace shapeapp.controller.commands
{
    export abstract class DelayedShapeCommand extends BaseShapeAppCommand
    {
        private _elipsedTime:number;
        constructor(model: IModel, view: IView)
        {
            super(model,view);
            this._elipsedTime = Date.now();
        }


        execute():void
        {
            let currentDelay:number = Date.now()-this._elipsedTime;
            if(currentDelay>=this.delay)
            {
                this._elipsedTime = Date.now();
                this.delayedExecute();
            }
        }

        protected abstract delayedExecute():void;

        protected abstract get delay():number;
    }
}