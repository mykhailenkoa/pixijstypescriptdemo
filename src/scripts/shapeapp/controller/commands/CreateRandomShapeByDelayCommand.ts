///<reference path="DelayedShapeCommand.ts"/>
namespace shapeapp.controller.commands
{
    import Point = PIXI.Point;

    export class CreateRandomShapeByDelayCommand extends DelayedShapeCommand
    {
        protected delayedExecute(): void
        {
            var indexesForRemove:number[] = this.view.getClickedOnShapesIndexesAndSetEmpty();
            var positionForNewShape:Point | null = this.view.getLastPositionForNewShapeAndSetEmpty();

            var hasAnyChanges:Boolean=false;
            var removedTotalSurfaceArea = this.view.removeShapesOutsideTheBox();
            if(removedTotalSurfaceArea>0){
                this.model.totalSurfaceArea -= removedTotalSurfaceArea;
                hasAnyChanges = true;
            }

            if(indexesForRemove.length>0)
            {
                this.model.totalSurfaceArea-=this.view.removeShapesByIndexes(indexesForRemove);

                hasAnyChanges=true;
            }

            if(positionForNewShape!==null)
            {
                this.model.generateRandomShapeByPosition(positionForNewShape);
                this.model.totalSurfaceArea+=this.view.drawNewShapes(this.model.generatedShapesToDraw);
                this.model.clearGeneratedShapes();
                hasAnyChanges= true;
            }

            if(hasAnyChanges) {
                this.view.updateNumberOfShapesHTMLDivInnerText(this.model.canvasNumberShapesDivName);
                this.view.updateTotalSurfaceAreaHTMLDivInnerText(this.model.canvasTotalSurfaceAreaDivName, Math.max(0,this.model.totalSurfaceArea));
            }
        }

        protected get delay():number
        {
            // 1 milisecods
            return 1;
        }

    }
}