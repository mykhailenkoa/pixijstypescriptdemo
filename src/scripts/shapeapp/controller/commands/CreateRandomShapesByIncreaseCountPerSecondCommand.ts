///<reference path="DelayedShapeCommand.ts"/>
namespace shapeapp.controller.commands {
  import Point = PIXI.Point;

  export class CreateRandomShapesByIncreaseCountPerSecondCommand extends DelayedShapeCommand {
    protected delayedExecute(): void {
      this.model.generateRandomShapes();
      this.model.totalSurfaceArea += this.view.drawNewShapes(
        this.model.generatedShapesToDrawDelayed
      );
      this.model.clearGeneratedShapesByDelay();
      this.view.updateNumberOfShapesHTMLDivInnerText(
        this.model.canvasNumberShapesDivName
      );
      this.view.updateTotalSurfaceAreaHTMLDivInnerText(
        this.model.canvasTotalSurfaceAreaDivName,
        Math.max(0, this.model.totalSurfaceArea)
      );
    }

    protected get delay(): number {
      return this.model.delayForCreateRndShape;
    }
  }
}
