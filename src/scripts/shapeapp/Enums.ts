namespace shapeapp.Enums
{
    export enum ShapeTypes
    {
        Circle,
        Ellipse,
        ThreeSides,
        FourSides,
        FifthSides,
        SixSides,
        SevenSides
    }
}