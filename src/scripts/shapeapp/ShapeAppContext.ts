///<reference path="./../shapeapp/model/ShapeAppModel.ts"/>
///<reference path="./../shapeapp/controller/ShapeAppController.ts"/>
///<reference path="view/PixiEngineViewExt.ts"/>
///<reference path="controller/BaseShapeAppCommand.ts"/>
///<reference path="controller/commands/ShapeAppFrameUpdateCommand.ts"/>
///<reference path="controller/commands/CreateRandomShapeByDelayCommand.ts"/>
///<reference path="controller/commands/CreateRandomShapesByIncreaseCountPerSecondCommand.ts"/>

///<reference path="controller/commands/ShapesCountDecteaseAtClickCmd.ts"/>
///<reference path="controller/commands/ShapesCountIncreaseAtClickCmd.ts"/>
///<reference path="controller/commands/GravityCountDecrease.ts"/>
///<reference path="controller/commands/GravityCountIncrease.ts"/>

import IController = core.interfaces.IController;
import IModel = core.interfaces.IModel;
import AbstractAppContext = core.implementations.AbstractAppContext;
import ShapeAppController = shapeapp.controller.ShapeAppController;
import ShapeAppModel = shapeapp.model.ShapeAppModel;
import PixiEngineViewExt = shapeapp.view.PixiEngineViewExt;
import ShapeAppFrameUpdateCommand = shapeapp.controller.commands.ShapeAppFrameUpdateCommand;
import CreateRandomShapeByDelayCommand = shapeapp.controller.commands.CreateRandomShapeByDelayCommand;
import CreateRandomShapesByIncreaseCountPerSecondCommand = shapeapp.controller.commands.CreateRandomShapesByIncreaseCountPerSecondCommand;
import ShapesCountDecteaseAtClickCmd = shapeapp.controller.commands.ShapesCountDecteaseAtClickCmd;
import ShapesCountIncreaseAtClickCmd = shapeapp.controller.commands.ShapesCountIncreaseAtClickCmd;
import GravityCountDecrease = shapeapp.controller.commands.GravityCountDecrease;
import GravityCountIncrease = shapeapp.controller.commands.GravityCountIncrease;
namespace shapeapp
{


    export class ShapeAppContext extends AbstractAppContext {

        //#region Private variables
        private _controller: ShapeAppController;
        private _view: PixiEngineViewExt;
        private _model: ShapeAppModel;
        private CORE_CMD_NS = core.implementations.commands;
        private APP_CMD_NS = shapeapp.controller;
        //#endregion

        //#region Public methods
        public initialize(): void
        {
            this.createAppController();
            this.createAppModel();
            this.createAppView();

            this.registerAppCommands();
        }

        public getModel(): IModel {
            return this._model;
        }

        public getView(): PixiEngineViewExt {
            return this._view;
        }

        public getController(): IController {
            return this._controller;
        }

        public onBeforeDeactivate(): void {
        }
        public onEnterFrameUpdate()
        {
            // Check if the application is not active, do not run
            if(!this._model.isAppActive)
                return;

            this._controller.executeCommand(this.APP_CMD_NS.CREATE_RND_SHAPE_BY_DELAY);
            this._controller.executeCommand(this.APP_CMD_NS.CREATE_RND_SHAPE_PER_SECOND_DELAY);
            this._controller.executeCommand(this.CORE_CMD_NS.APP_VIEW_REDRAW);
        }
        //#endregion
        public executeExternalCommand(name:string):void
        {
            this._controller.executeCommand(name);
        }
        //region Private methods
        private createAppView(): void
        {
            this._view = new PixiEngineViewExt(this._model.canvasWidth,
                this._model.canvasHeight,
                document.getElementById(this._model.canvasHTMLElementDivName)
            );
        }

        private createAppModel(): void
        {
            this._model = new ShapeAppModel();
        }

        private createAppController(): void
        {
            this._controller = new ShapeAppController(this);
        }

        private registerAppCommands(): void
        {
            // Register core commands
            this.registerCoreCommands( new Map<string, Function>(
                [
                    [this.CORE_CMD_NS.APP_VIEW_REDRAW,ShapeAppFrameUpdateCommand]
                ]
            ) );

            //Register application commands
            this._controller.registerCommand(this.APP_CMD_NS.CREATE_RND_SHAPE_BY_DELAY, CreateRandomShapeByDelayCommand);
            this._controller.registerCommand(this.APP_CMD_NS.CREATE_RND_SHAPE_PER_SECOND_DELAY, CreateRandomShapesByIncreaseCountPerSecondCommand);
            this._controller.registerCommand(this.APP_CMD_NS.SHAPES_COUNT_DECREASE_ATCLICK, ShapesCountDecteaseAtClickCmd);
            this._controller.registerCommand(this.APP_CMD_NS.SHAPES_COUNT_INCREASE_ATCLICK, ShapesCountIncreaseAtClickCmd);
            this._controller.registerCommand(this.APP_CMD_NS.GRAVITY_COUNT_DECREASE, GravityCountDecrease);
            this._controller.registerCommand(this.APP_CMD_NS.GRAVITY_COUNT_INCREASE,GravityCountIncrease);

            this._view.addInteractiveEvents();
        }

        //endregion

    }

////window HTML static handlers
const shapeAppContext:ShapeAppContext = new ShapeAppContext();
window.onload = function () {
    shapeAppContext.initialize();
    requestAnimationFrameUpdate();
    document.getElementById("ShapesCountDecrease").onmousedown = shapesCountDecreaseOnClick;
    document.getElementById("ShapesCountIncrease").onmousedown = shapesCountIncreaseOnClick;

    document.getElementById("GravityCountDecrease").onmousedown = gravityCountDecreaseOnClick;
    document.getElementById("GravityCountIncrease").onmousedown = gravityCountIncreaseOnClick;
};
window.onresize = function () {

};
function requestAnimationFrameUpdate()
{
    window.requestAnimationFrame(requestAnimationFrameUpdate);
    shapeAppContext.onEnterFrameUpdate();
}

function shapesCountDecreaseOnClick()
{
    shapeAppContext.executeExternalCommand(shapeapp.controller.SHAPES_COUNT_DECREASE_ATCLICK);
}
function shapesCountIncreaseOnClick()
{
    shapeAppContext.executeExternalCommand(shapeapp.controller.SHAPES_COUNT_INCREASE_ATCLICK);
}
function gravityCountDecreaseOnClick()
{
    shapeAppContext.executeExternalCommand(shapeapp.controller.GRAVITY_COUNT_DECREASE);
}
function gravityCountIncreaseOnClick()
{
    shapeAppContext.executeExternalCommand(shapeapp.controller.GRAVITY_COUNT_INCREASE);
}
////
}