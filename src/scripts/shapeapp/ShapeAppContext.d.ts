import IController = core.interfaces.IController;
import IModel = core.interfaces.IModel;
import IView = core.interfaces.IView;
import AbstractAppContext = core.implementations.AbstractAppContext;
export declare class ShapeAppContext extends AbstractAppContext {
    private _controller;
    private _view;
    private _model;
    initialize(): void;
    getModel(): IModel;
    getView(): IView;
    getController(): IController;
    OnBeforeDeactivate(): void;
    private createAppView();
    private createAppModel();
    private createAppController();
    private registerAppCommands();
}
