///<reference path="./../../../../node_modules/pixi-typescript/pixi.js.d.ts"/>

namespace shapeapp.view {
  import Application = PIXI.Application;
  import Sprite = PIXI.Sprite;
  import Container = PIXI.Container;
  import Graphics = PIXI.Graphics;
  import Text = PIXI.Text;
  import Point = PIXI.Point;
  import InteractionEvent = PIXI.interaction.InteractionEvent;
  import Rectangle = PIXI.Rectangle;
  import AbstractPixiShapeVO = shapeapp.model.vo.AbstractPixiShapeVO;

  export class PixiEngineViewExt implements IView {
    private _app: Application;

    private _shapes: shapeContainer[];

    private _stage: Container;
    private _fpsMetter: Text;

    private _gravityVal: number = 1;
    private _maxPosY: number;
    private _shapesCount: number = 0;
    private _maxPosX: number;

    private _clickCountOnEmptySpace: Point[];
    private _clickedOnShapesIndexes: number[];

    constructor(canvasW: number, canvasH: number, canvasNode: Node) {
      this._shapes = [];

      this._app = new Application(canvasW, canvasH, {
        backgroundColor: 0x000000,
        autoStart: true,
      });
      this._app.view.style.position = 'absolute';
      this._app.view.style.display = 'block';

      canvasNode.appendChild(this._app.view);

      this._stage = new Container();

      this._fpsMetter = new Text('FPS: 60.00', {
        fontFamily: 'Arial',
        fontSize: 16,
        fill: 'white',
      });
      const intent: number = 10;

      this._fpsMetter.position.set(
        canvasW - this._fpsMetter.width - intent,
        canvasH - this._fpsMetter.height - intent
      );
      this._stage.addChild(this._fpsMetter);

      this._maxPosX = canvasW;
      this._maxPosY = canvasH;
      this._stage.interactive = this._stage.interactiveChildren = true;

      this._stage.hitArea = new Rectangle(0, 0, canvasW, canvasH);
      this._clickedOnShapesIndexes = [];
      this._clickCountOnEmptySpace = [];
    }
    public addInteractiveEvents() {
      this._stage.addListener(
        'pointerdown',
        this.stageInteractivePointerUp,
        this
      );
    }
    /**
     *  TODO on resize IView implementation
     *  @inheritDoc
     */
    public resize(w: number, h: number): void {
      if (this._app.renderer.width != w || this._app.renderer.height != h) {
        this._app.renderer.resize(w, h);
        console.log('PixiEngineView=>resize: width' + w + ', height:' + h);
        this._maxPosX = w;
        this._maxPosY = h;
      }
    }

    /**
     *  @inheritDoc
     */
    public onEnterFrameRedraw(): void {
      this.updateShapesPosY();
      this._app.renderer.render(this._stage);
    }

    public drawNewShapes(shapesVO: AbstractPixiShapeVO[]): number {
      var addedTotalSurfaceArea: number = 0;

      for (let shapeVO of shapesVO) {
        addedTotalSurfaceArea += shapeVO.surfaceArea;
        this.drawNewShape(shapeVO);
      }
      return addedTotalSurfaceArea;
    }
    public removeShapesByIndexes(indexes: number[]): number {
      var removedTotalSurfaceArea: number = 0;
      let forRemove: shapeContainer[] = this._shapes.filter(
        (value: shapeContainer, index: number, array: shapeContainer[]) => {
          return indexes.indexOf(index) >= 0;
        }
      );
      var toRemove: shapeContainer;
      const removedShapesVO: AbstractPixiShapeVO[] = [];
      while (forRemove.length) {
        toRemove = forRemove.pop();
        removedTotalSurfaceArea += toRemove.vo.surfaceArea;
        removedShapesVO[removedShapesVO.length] = this._shapes.splice(
          this._shapes.indexOf(toRemove),
          1
        )[0].vo;
        toRemove.display.destroy({
          children: true,
          texture: true,
          baseTexture: true,
        });
      }
      this._shapesCount = this._shapes.length;
      this.findShapesByTypeAndReColorThem(removedShapesVO);
      return removedTotalSurfaceArea;
    }
    private findShapesByTypeAndReColorThem(
      shapes: AbstractPixiShapeVO[]
    ): void {
      if (shapes.length > 0) {
        const shapeTypes = shapes.map((shape) => shape.shapeType);
        const shpesForUpdate = this._shapes.filter((shape) =>
          shapeTypes.includes(shape.vo.shapeType)
        );
        for (const shape of shpesForUpdate) {
          const shapeVO = shape.vo;
          const display = shape.display;
          shapeVO.randomizeColors();
          let graphics: Graphics = new Graphics();
          graphics.lineStyle(
            shapeVO.lineWidth,
            shapeVO.lineColor,
            shapeVO.lineAlpha
          );
          graphics.beginFill(shapeVO.fillColor, shapeVO.fillAlpha);
          graphics.drawShape(shapeVO.shape);
          graphics.endFill();
          display.texture = graphics.generateCanvasTexture();
          graphics.destroy();
        }
      }
    }
    /**
     *  @inheritDoc
     */
    public updateGravityValue(value: number): void {
      this._gravityVal = value;
    }

    /**
     * @inheritDoc
     */
    public dispose(): void {
      this._stage.removeChildren();
      delete this._stage;
    }

    /**
     * @inheritDoc
     */
    public setEnabled(value: Boolean): void {
      if (value) this._app.start();
      else this._app.stop();
    }

    public updateDisplayFPS(fps: number): void {
      this._fpsMetter.text = 'FPS: ' + fps.toFixed(2).toString();
    }

    public updateNumberOfShapesHTMLDivInnerText(htmlDivName: string) {
      document.getElementById(htmlDivName).innerText =
        'Shapes count:' + this._shapesCount;
    }

    public updateTotalSurfaceAreaHTMLDivInnerText(
      htmlDivName: string,
      totalSurfaceArea: number
    ): void {
      document.getElementById(htmlDivName).innerText =
        'Total surface area:' +
        Math.floor(totalSurfaceArea).toFixed(0).toString();
    }
    public getClickedOnShapesIndexesAndSetEmpty(): number[] {
      return this._clickedOnShapesIndexes.splice(
        0,
        this._clickedOnShapesIndexes.length
      );
    }
    public getPositionsForNewShapesAndSetEmpty(): Point[] {
      return this._clickCountOnEmptySpace.splice(
        0,
        this._clickCountOnEmptySpace.length
      );
    }
    public getLastPositionForNewShapeAndSetEmpty(): Point | null {
      return this._clickCountOnEmptySpace.length > 0
        ? this._clickCountOnEmptySpace.pop()
        : null;
    }
    private drawNewShape(shapeVO: AbstractPixiShapeVO): void {
      let graphics: Graphics = new Graphics();
      graphics.lineStyle(
        shapeVO.lineWidth,
        shapeVO.lineColor,
        shapeVO.lineAlpha
      );
      graphics.beginFill(shapeVO.fillColor, shapeVO.fillAlpha);
      graphics.drawShape(shapeVO.shape);
      graphics.endFill();

      let newSprite: Sprite = new Sprite(graphics.generateCanvasTexture());
      graphics.destroy();

      newSprite.x = shapeVO.position.x - newSprite.width * 0.5;
      newSprite.y = shapeVO.position.y - newSprite.height * 0.5;

      this._stage.addChild(newSprite);

      this._shapes.push({ display: newSprite, vo: shapeVO });

      this._shapesCount = this._shapes.length;
    }
    private stageInteractivePointerUp(event: InteractionEvent): void {
      let point: Point = event.data.global.clone();
      var onShapeClicked: boolean = false;
      var shapeBounds: Rectangle;

      for (var i: number = 0; i < this._shapesCount; i++) {
        shapeBounds = this._shapes[i].display.getBounds();
        if (shapeBounds.contains(point.x, point.y)) {
          onShapeClicked = true;
          this._clickedOnShapesIndexes.push(i);
        }
      }

      if (!onShapeClicked) {
        this._clickCountOnEmptySpace.push(point);
      }
    }
    private updateShapesPosY(): void {
      this._shapes.forEach(
        (sp: shapeContainer, index: number, orig: shapeContainer[]) => {
            sp.display.position.y += this._gravityVal;
        }
      );
    }
    public removeShapesOutsideTheBox(): number{
      const forRemove = this._shapes.filter(shape=>(shape.display.position.y + shape.display.height > this._maxPosY));
      var removedTotalSurfaceArea: number = 0;
      var toRemove: shapeContainer;
      const removedShapesVO: AbstractPixiShapeVO[] = [];
      while (forRemove.length) {
        toRemove = forRemove.pop();
        this._shapes.splice(this._shapes.findIndex(shape=>toRemove===shape),1)
        removedTotalSurfaceArea += toRemove.vo.surfaceArea;
        toRemove.display.destroy({
          children: true,
          texture: true,
          baseTexture: true,
        });
      }
      this._shapesCount = this._shapes.length;
      return removedTotalSurfaceArea;    
    }
  }

  //Helped type container (ShapeContainer )
  interface ShapeContainer {
    display: Sprite;
    vo: AbstractPixiShapeVO;
  }
  declare type shapeContainer = ShapeContainer;
}
