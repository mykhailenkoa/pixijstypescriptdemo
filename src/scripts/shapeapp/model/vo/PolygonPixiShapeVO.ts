namespace shapeapp.model.vo
{
    import Polygon = PIXI.Polygon;
    import ShapeTypes = shapeapp.Enums.ShapeTypes;
    import Point = PIXI.Point;

    export class PolygonPixiShapeVO extends AbstractPixiShapeVO
    {
        protected _maxRadius:number = 50;
        protected _minRadius:number = 10;
        constructor(polygonCount:number, shapeType:ShapeTypes, radius?:number|undefined)
        {
            super();
            this._polygonCount=polygonCount;
            this._shapeType =shapeType;
            this.generateShape(radius);
        }

        private _radius:number;
        private _polygonCount:number;
        private _shape:Polygon;
        private _surfaceArea:number;
        private _shapeType:ShapeTypes;

        public get shape():Polygon
        {
            return this._shape;
        }

        public get shapeType():ShapeTypes
        {
            return this._shapeType;
        }
        public get surfaceArea():number
        {
            return this._surfaceArea;
        }
        public clone( withRundomValue:boolean ):AbstractPixiShapeVO
        {
            var clone = new PolygonPixiShapeVO(this._polygonCount,this._shapeType,withRundomValue?undefined:this._radius);

            if(withRundomValue)
            {
                clone.randomizeColors();
                clone.lineWidth = 1 + Math.random()*clone._radius/3;
            }
            else {
                clone.fillColor = this.fillColor;
                clone.fillAlpha = this.fillAlpha;

                clone.lineColor = this.lineColor;
                clone.lineAlpha = this.lineAlpha;

                clone.position.copy(this.position);
            }

            return clone;
        }

        private generateShape(radius?:number|undefined):void
        {
            this._radius = (typeof radius == "undefined")?(this._minRadius +Math.random()*(this._maxRadius-this._minRadius)):radius;
            var z:number = 0;
            var i:number=0;
            var points:Point[] =[];
            var angle:number = 360.0/this._polygonCount;

            while (i<=this._polygonCount)
            {
                points.push(new Point(
                this._radius + Math.round(Math.cos(z/180 * Math.PI)*this._radius),
                this._radius - Math.round(Math.sin(z/180 * Math.PI)*this._radius)));
                z+=angle;
                i++;
            }
            this._shape = new Polygon( points );

            this._surfaceArea = this._radius*this._radius*this._polygonCount*Math.PI;
        }
    }
}