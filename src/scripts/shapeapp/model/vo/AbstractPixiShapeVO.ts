
namespace shapeapp.model.vo
{
    import Point = PIXI.Point;
    import ShapeTypes = shapeapp.Enums.ShapeTypes;

    export abstract class AbstractPixiShapeVO
    {

        public lineColor:number = 0x000000;
        public lineAlpha:number = 1.0;
        public lineWidth:number = 2.0;
        /**
         * Fill color value
         * @type {number}
         */
        public fillColor:number = 0x8CFF68;
        /**
         * Fill alpha value
         * @type {number}
         */
        public fillAlpha:number = 1.0;

        public position:Point = new Point();

        public  randomizeColors():void
        {
            this.fillColor = Math.random()*0xFFFFFF;
            this.lineColor = Math.random()*0XFFFFFF;
        }
        public abstract get shape():any;
        public abstract get shapeType():ShapeTypes;
        public abstract get surfaceArea():number;
        public abstract clone( withRundomValue:boolean ):AbstractPixiShapeVO;

    }
}