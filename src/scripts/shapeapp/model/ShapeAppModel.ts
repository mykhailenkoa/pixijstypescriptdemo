///<reference path="vo/CirclePixiShapeVO.ts"/>
///<reference path="vo/EllipseShapeVO.ts"/>
///<reference path="vo/PolygonPixiShapeVO.ts"/>

import AbstractAppModel = core.implementations.AbstractAppModel;

namespace shapeapp.model {
  import AbstractPixiShapeVO = shapeapp.model.vo.AbstractPixiShapeVO;
  import CirclePixiShapeVO = shapeapp.model.vo.CirclePixiShapeVO;
  import Point = PIXI.Point;
  import EllipseShapeVO = shapeapp.model.vo.EllipseShapeVO;
  import PolygonPixiShapeVO = shapeapp.model.vo.PolygonPixiShapeVO;
  import ShapeTypes = shapeapp.Enums.ShapeTypes;

  export class ShapeAppModel extends AbstractAppModel {
    private _gravityVal: number = 1;
    // By click
    private _generatedShapesToDraw: AbstractPixiShapeVO[];
    // By delay/per second
    private _generatedShapesToDrawDelayed: AbstractPixiShapeVO[];
    private _fpsMetter: fpsMeterVO;
    private _shapesPatternVO: AbstractPixiShapeVO[];
    private _multiplierOfCreation: number = 1;
    private _totalSurfaceArea: number = 0;
    constructor() {
      super();
      this.isAppActive = true;
      this._fpsMetter = {
        nbFrames: 0,
        totalFPS: 60,
        frameRate: 0.0,
        elapsed: performance.now(),
        refresh: 600,
      };
      this.initializeShapePatterns();
    }
    public get multiplierOfCreation(): number {
      return this._multiplierOfCreation;
    }

    public set multiplierOfCreation(value: number) {
      this._multiplierOfCreation = value;
    }

    public get totalSurfaceArea(): number {
      return this._totalSurfaceArea;
    }

    public set totalSurfaceArea(value: number) {
      this._totalSurfaceArea = value;
    }
    public get gravityVal(): number {
      return this._gravityVal;
    }

    public set gravityVal(value: number) {
      this._gravityVal = value;
    }

    /**
     * @inheritDoc
     */
    public get canvasHTMLElementDivName(): string {
      return 'GameCanvas';
    }

    public get canvasNumberShapesDivName(): string {
      return 'NumberOfShapes';
    }

    public get canvasTotalSurfaceAreaDivName(): string {
      return 'TotalSurfaceArea';
    }

    public get generatedShapesToDraw(): AbstractPixiShapeVO[] {
      return this._generatedShapesToDraw;
    }
    public get generatedShapesToDrawDelayed(): AbstractPixiShapeVO[]{
        return this._generatedShapesToDrawDelayed;
    }
    public clearGeneratedShapes(): void {
      this._generatedShapesToDraw.length = 0;
    }
    public clearGeneratedShapesByDelay(): void {
        this._generatedShapesToDrawDelayed.length=0;
    }
    public generateRandomShapes(): void {
      var generatedShape: AbstractPixiShapeVO;
      var multiply: number;
      var halfSize: number;
      multiply = this._multiplierOfCreation;

      while (multiply-- > 0) {
        generatedShape = this.generateRandomShape();
        halfSize = Math.sqrt(generatedShape.surfaceArea) * 0.5;
        generatedShape.position.x =
          halfSize + Math.random() * (this._canvasWidth - halfSize);
        generatedShape.position.y =
          -halfSize;
        this._generatedShapesToDrawDelayed.push(generatedShape);
      }
    }
    public generateRandomShapeByPosition(position: Point): void {
      var generatedShape: AbstractPixiShapeVO;
      var size: number;

      generatedShape = this.generateRandomShape();
      const pos = position.clone();

      size = Math.sqrt(generatedShape.surfaceArea) / 2;
      if (pos.x + size < this._canvasWidth - size) {
        pos.x += size;
      } else if (pos.x - size > 0) {
        pos.x -= size;
      } else if (pos.y + size < this._canvasHeight - size) {
        pos.y += size;
      } else if (pos.y - size > 0) {
        pos.y -= size;
      }
      generatedShape.position.copy(pos);
      this._generatedShapesToDraw.push(generatedShape);
    }
    public calculateBeforeViewRender() {
      this._fpsMetter.elapsed = performance.now();
    }
    public calculateAfterViewRender() {
      let now = performance.now();
      let frameTime = now - this._fpsMetter.elapsed;

      this._fpsMetter.nbFrames++;
      if (frameTime >= this._fpsMetter.refresh) {
        console.log(
          'calculateAfterViewRender=>frameTime>=this._fpsMetter.refresh'
        );
        this._fpsMetter.totalFPS =
          (1000.0 * this._fpsMetter.nbFrames) / frameTime;
        this._fpsMetter.elapsed = now;
        this._fpsMetter.nbFrames = 0;
      }
    }

    public get calculatedFPS(): number {
      return this._fpsMetter.totalFPS;
    }

    /**
     * Current delay number value,
     * for create random shape by delayed
     * time in a miliseconds
     * @returns {number}
     */
    public get delayForCreateRndShape(): number {
      //TODO
      // need move to application config,
      // for now hardcoded  value, please change if you want,
      // value in a milliseconds

      return 1000;
    }

    private initializeShapePatterns(): void {
      this._shapesPatternVO = [
        new CirclePixiShapeVO(),
        new EllipseShapeVO(),
        new PolygonPixiShapeVO(3, ShapeTypes.ThreeSides, undefined),
        new PolygonPixiShapeVO(4, ShapeTypes.FourSides, undefined),
        new PolygonPixiShapeVO(5, ShapeTypes.FifthSides, undefined),
        new PolygonPixiShapeVO(6, ShapeTypes.SixSides, undefined),
        new PolygonPixiShapeVO(7, ShapeTypes.SevenSides, undefined),
      ];
      this._generatedShapesToDraw = [];
      this._generatedShapesToDrawDelayed = [];
    }

    private generateRandomShape(): AbstractPixiShapeVO {
      return this._shapesPatternVO[
        Math.floor(this._shapesPatternVO.length * Math.random())
      ].clone(true);
    }
  }

  /**
   * Help model interfaces
   */
  interface FPSMeterVO {
    nbFrames: number;
    frameRate: number;
    elapsed: number;
    refresh: number;
    totalFPS: number;
  }
  declare type fpsMeterVO = FPSMeterVO;
}
